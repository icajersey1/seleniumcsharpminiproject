﻿using System;
using ICAMini.RepositoryClass;
using NUnit.Framework;

namespace ICAMini.TestCaseClass
{
    public class Calculator : BaseClass
    {
        /// Calling Addition method using object of ReactJSCalculatorRepo class
        /// Author : Ruma
        /// Since : 8th July 2020

        [Test]
        public void Add()
        {

            ReactJSCalculatorRepo calc = new ReactJSCalculatorRepo(driver);
            calc.Addition();
        }
    }
}
