﻿using System;
using NUnit.Framework;
using ICAMini;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;
using System.Configuration;

namespace ICAMini
{
    [TestFixture]
    public class LoginStep : BaseClass
    {

        /// Facebook Login calling methods of LoginPageRepo class
        /// Author : Ruma
        /// Since : 8th July 2020

        [Test]
        public void UserLogin()
        {
            LoginPageRepo obj = new LoginPageRepo(driver);
            String fusername = configread.GetUsername();
            String fpassword = configread.GetPassword();


            obj.EnterUsername(fusername);
           Thread.Sleep(3000);
           obj.EnterPassword(fpassword);
           Thread.Sleep(3000);
           obj.Login();
           Thread.Sleep(5000);
           
        }
        
        
      
    }
}
