﻿using System;
using NUnit.Framework;
using ICAMini;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;
using System.Configuration;

namespace ICAMini
{
    [TestFixture]
    public class ViewProfile : BaseClass

    {
        /// Test Order , Author specified using NUnit attributes and ignoring the test case using assertions
        /// Facebook Login
        /// Author : Ruma
        /// Since : 8th July 2020

        [Test,Order(0)]
        [Author("Ruma Dey","rumadey12@yahoo.com")]
        public void UserLogin()
        {
            Assert.Ignore();
            LoginPageRepo obj = new LoginPageRepo(driver);
            String fusername = configread.GetUsername();
            String fpassword = configread.GetPassword();


            obj.EnterUsername(fusername);
            Thread.Sleep(3000);
            obj.EnterPassword(fpassword);
            Thread.Sleep(3000);
            obj.Login();
            Thread.Sleep(10000);

        }

        
        /// Facebook Login, Clicking on Profile icon and validation of the profile page
        /// Author : Ruma
        /// Since : 8th July 2020

        [Test,Order(1)]
        public void Profile()
        {
       

            var vwpr = new FacebookHomePageRepo(driver);
            LoginPageRepo obj = new LoginPageRepo(driver); 

            String fusername = configread.GetUsername();
            String fpassword = configread.GetPassword();


            obj.EnterUsername(fusername); 
            Thread.Sleep(3000);
          
            obj.EnterPassword(fpassword);  
            Thread.Sleep(3000);
           
            obj.Login();   
            Thread.Sleep(10000);
            
            vwpr.ViewProfile(); 
            Thread.Sleep(3000);
            
        }
    }
}
