﻿using System;
using System.Collections.Generic;
using ICAMini;
using ICAMini.RepositoryClass;
using NUnit.Framework;

namespace ICAMini.TestCaseClass
{
    public class GoogleSearch : BaseClass
    {
        /// Calling Search method using object of GooglePage class
        /// Author : Ruma
        /// Since : 8th July 2020

        [Test]
        public void search()
        {
            GooglePage gp = new GooglePage(driver);
            gp.GoogleSearch();
        }

       

    }
}
