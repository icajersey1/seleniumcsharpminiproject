﻿using System;
using NUnit.Framework;
using ICAMini;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;


namespace ICAMini
{

    public class LoginPageRepo
    {
        
        IWebDriver driver = null;

        /// WebDriver Instantiation via Constructor Class
        /// Author : Ruma
        /// Since : 8th July 2020

        public LoginPageRepo (IWebDriver driver)
        {

            this.driver = driver;
        }

        /// Locating WebElement username box in facebook login page via get method
        /// Author : Ruma
        /// Since : 8th July 2020

        public IWebElement username
        {
            get
            {
                return this.driver.FindElement(By.Id("email"));

            }

        }

        /// Locating WebElement password box in facebook login page via get method
        /// Author : Ruma
        /// Since : 8th July 2020

        public IWebElement password
        {
            get
            {
                return this.driver.FindElement(By.Id("pass"));

            }

        }

        /// Locating WebElement login button in facebook login page via get method
        /// Author : Ruma
        /// Since : 8th July 2020

        public IWebElement login
        {
            get
            {
                return this.driver.FindElement(By.Id("u_0_b"));

            }

        }

        /// Feeding username against username box 
        /// Author : Ruma
        /// Since : 8th July 2020

        public void EnterUsername(String fusername)
        {

            username.SendKeys(fusername);
           

        }

        /// Feeding username against password box 
        /// Author : Ruma
        /// Since : 8th July 2020

        public void EnterPassword(String fpassword)

        {
            
            password.SendKeys(fpassword);
            
        }


        /// Clicking on Login button
        /// Author : Ruma
        /// Since : 8th July 2020

        public void Login()

        {
            
            login.Click();
           

        }
    }
}
