﻿using System;
using NUnit.Framework;
using ICAMini;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;


namespace ICAMini
{
    public class FacebookHomePageRepo
    {
      
      IWebDriver driver = null;


        /// WebDriver Instantiation via Constructor Class
        /// Author : Ruma
        /// Since : 8th July 2020
        
        public FacebookHomePageRepo(IWebDriver driver)
            {

                this.driver = driver;
            }

        /// Locating WebElement facebook profile icon
        /// Author : Ruma
        /// Since : 8th July 2020

        public IWebElement profile
        {
            get
            {
                return this.driver.FindElement(By.XPath("//a[@href=\"/me/\"]"));
            }

        }

        /// Clicking on facebook profile icon
        /// Author : Ruma
        /// Since : 8th July 2020

        public void ViewProfile()
        {
            Console.WriteLine(profile.Displayed);
            profile.Click();
            Assert.AreEqual("Ruma", profile.Text);
        }
    }
}
