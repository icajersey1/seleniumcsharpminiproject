﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ICAMini;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;


namespace ICAMini.RepositoryClass
{
    public class GooglePage
    {
        public IWebDriver driver = null;

        /// WebDriver Instantiation via Constructor Class
        /// Author : Ruma
        /// Since : 8th July 2020

        public GooglePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        /// Locating WebElement Google Search Box
        /// Author : Ruma
        /// Since : 8th July 2020

        public IWebElement searchbox   
        {
            get

            {
                return this.driver.FindElement(By.XPath("//input[@role='combobox']"));

            }

        }


        /// Extracting dynamically generated search values in google search box using dynamic xpath
        /// Author : Ruma
        /// Since : 8th July 2020

        public void GoogleSearch()

        {
            searchbox.SendKeys("selenium");
            Thread.Sleep(10000);

            IList<IWebElement> list = driver.FindElements
                (By.XPath("//ul[@role='listbox']/li/descendant::div[@class='sbl1']"));
            
            Console.WriteLine("Total search items that have appeared are" + list.Count);

        foreach(var item in list)
            {
                Console.WriteLine(item);
                Thread.Sleep(10000);

                if (item.Equals("selenium tutorial"))

                    item.Click();

            }
        }
    }
}
