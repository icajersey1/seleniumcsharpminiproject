﻿using System;
using ICAMini;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;


namespace ICAMini.RepositoryClass
{
    public class ReactJSCalculatorRepo
    {
            IWebDriver driver = null;

        /// WebDriver Instantiation via Constructor Class
        /// Author : Ruma
        /// Since : 8th July 2020
        
        public ReactJSCalculatorRepo(IWebDriver driver)
            {

                this.driver = driver;
            }


        /// Locating WebElement 7 in ReactJS Calculator page via get method
        /// Author : Ruma
        /// Since : 8th July 2020

        public IWebElement Digit7
        {
            get
            {
                return this.driver.FindElement(By.XPath("//div[@class='component-button-panel']/child::div/div/button[contains(text(),'7')]"));
            }

        }

        /// Locating WebElement 3 in ReactJS Calculator page via get method
        /// Author : Ruma
        /// Since : 8th July 2020

        public IWebElement Digit3
        {
            get
            {
                return this.driver.FindElement(By.XPath("//div[@class='component-button-panel']/div[5]/preceding-sibling::div/div/button[contains(text(),'3')]"));
            }

        }

        /// Locating WebElement + in ReactJS Calculator page via get method
        /// Author : Ruma
        /// Since : 8th July 2020

        public IWebElement SignPlus
        {
            get
            {
                return this.driver.FindElement(By.XPath("//div[@class='component-button-panel']/child::div/div[@class='component-button orange']/button[contains(text(),'+')]"));
            }

        }

        /// Locating WebElement = in ReactJS Calculator page via get method
        /// Author : Ruma
        /// Since : 8th July 2020

        public IWebElement SignEqualTo
        {
            get
            {
                return this.driver.FindElement(By.XPath("//div[@class='component-button-panel']/child::div/div[@class='component-button orange']/button[contains(text(),'=')]"));
            }

        }

        /// Locating WebElement result panel in ReactJS Calculator page via get method
        /// Author : Ruma
        /// Since : 8th July 2020

        public IWebElement CalculationResult
        {
            get
            {
                return this.driver.FindElement(By.XPath("//div[@class='component-display']/child::div[1]"));
            }

        }


        /// Addition of two digits and validating the result displayed
        /// Author : Ruma
        /// Since : 8th July 2020

        public void Addition()
        {
            Console.WriteLine("7 is present >>" + Digit7.Displayed + " | Digit is >> " + Digit7.Text);
            Digit7.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            Console.WriteLine("+ is present >>" + SignPlus.Displayed + " | Sign  is >> " + SignPlus.Text);
            SignPlus.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);


            Console.WriteLine("3 is present >>" + Digit3.Displayed + " | Digit is >> " + Digit3.Text);
            Digit3.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            
            Console.WriteLine("= is present >>" + SignEqualTo.Displayed + " | Sign is >> " + SignEqualTo.Text);
            SignEqualTo.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            Console.WriteLine("Calculation Result :  " + CalculationResult.Displayed + " | Result is >> " + CalculationResult.Text);


            
            
        }
    }
    }

