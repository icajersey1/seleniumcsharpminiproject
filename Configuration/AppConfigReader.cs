﻿using System;
using System.Configuration;

namespace ICAMini.Configuration
{
    public class AppConfigReader : IConfig
    {
        /// Reading Url value from config file
        /// Author : Ruma
        /// Since : 8th July 2020

        public String FetchUrl()
        {
            return ConfigurationManager.AppSettings.Get("url1");
        }


        /// Reading Browser value from config file and parsing it to enum
        /// Author : Ruma
        /// Since : 8th July 2020
        
        public BrowserEnum GetBrowser()
        {
            String browser = ConfigurationManager.AppSettings.Get("browserName");
            return (BrowserEnum)Enum.Parse(typeof(BrowserEnum), browser);
        }


        /// Reading Password value from config file 
        /// Author : Ruma
        /// Since : 8th July 2020
        
        public String GetPassword()
         {
            return ConfigurationManager.AppSettings.Get("password");
         }

        /// Reading Username value from config file 
        /// Author : Ruma
        /// Since : 8th July 2020

        public String GetUsername()
         {
            return ConfigurationManager.AppSettings.Get("username");
         }



    }

}