﻿using System;

namespace ICAMini.Configuration
{

    /// BrowserEnum Class storing various browser types
    /// Author : Ruma
    /// Since : 8th July 2020
    
    public enum BrowserEnum
    {
        chrome,
        ie,
        firefox,
        safari,
        edge
    }
}
