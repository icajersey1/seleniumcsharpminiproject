﻿using System;
namespace ICAMini.Configuration
{

    /// IConfig Interface containing methods for AppConfigReader
    /// Author : Ruma
    /// Since : 8th July 2020

    public interface IConfig
    {
        BrowserEnum GetBrowser();

        string GetUsername();

        string GetPassword();

        string FetchUrl();
    }
}
