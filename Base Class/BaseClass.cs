﻿using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Safari;
using System.Collections.Generic;
using System.Configuration;
using ICAMini.Configuration;

namespace ICAMini
{

    [TestFixture]
    public class BaseClass
    {
        public IWebDriver driver = null;
        public AppConfigReader configread = new AppConfigReader();


        /// CrossBrowser Url Launching Implemented and Url,Page Source and Title Length
        /// Author : Ruma
        /// Since : 8th July 2020

        [SetUp]
        public void SetUp()

        {
            
            BrowserEnum browser = configread.GetBrowser();
            String browserName = browser.ToString();
            String UrlAdress = configread.FetchUrl();

            if (browserName.Equals("chrome"))
                driver = new ChromeDriver();

            else if (browserName.Equals("ie"))
                driver = new InternetExplorerDriver();

            else if (browserName.Equals("firefox"))
                driver = new FirefoxDriver();

            else if (browserName.Equals("safari"))
                driver = new SafariDriver();

            
            driver.Url = UrlAdress;

            Console.WriteLine("Current Page Url Length is : " + driver.Url.Length);
            Console.WriteLine("Current Page Title is : " + driver.Title);
            Console.WriteLine("Current Page Title Length is : " + driver.Title.Length);
            Console.WriteLine("Current Page Source is : " + driver.PageSource);


            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);

        }


        /// Browser Window Closing
        /// Author : Ruma
        /// Since : 8th July 2020
        
        [TearDown]

        public void CloseBrowser()

        {
            driver.Quit();

        }

        

    }
}
