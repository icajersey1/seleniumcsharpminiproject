﻿using System;
using System.Collections;
using System.Drawing.Imaging;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ICAMini.AdhocFolder
{
    public class DataDriven_Test_Case
    {
        /// For Providing data source via TestCaseSource attribute
        /// Author : Ruma
        /// Since : 8th July 2020

        [Test]
        [TestCaseSource("DataDrivenTesting")]
        public void TestCase(string UrlName, string screenshotName)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Url = UrlName;
            Thread.Sleep(3000);
            ITakesScreenshot ts = driver as ITakesScreenshot;
            Screenshot scsh = ts.GetScreenshot();
            string pth = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string finalpth = pth.Substring(0, pth.LastIndexOf("bin")) + "Screenshots/" + screenshotName;
            string localpth = new Uri(finalpth).LocalPath;
         //   scsh.SaveAsFile(localpth, ImageFormat.Jpeg);

            
        }

        /// Storing data sources using Collections
        /// Author : Ruma
        /// Since : 8th July 2020
        
        static IList DataDrivenTesting()
        {
            ArrayList list = new ArrayList();
            list.Add("http://www.youtube.com");
            list.Add("http://www.gmail.com");
            list.Add("http://www.yahoo.com");

            return list;
        }
    }
}
