﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ICAMini.AdhocScenarios
{
    public class DropdownandMultiSelectOperations
    {
        [Test]
        public void Dropdown()
        {

            // Create a new instance of the Chrome driver
            IWebDriver driver = new ChromeDriver();

            // Put an Implicit wait, this means that any search for elements on the page could take the time the implicit wait is set for before throwing exception
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // Launch the URL
            driver.Url = "https://demoqa.com/automation-practice-form";
            driver.Manage().Window.Maximize();

            IJavaScriptExecutor executor = driver as IJavaScriptExecutor;
            executor.ExecuteScript(String.Format("window.scrollTo({0},{1})",0,707));
            Thread.Sleep(7000);

            // Step 3: Select 'State' Drop down 
            // Select Dropdown options using Actions class and Fluent wait


           IWebElement selectState = driver.FindElement(By.XPath("//div[@id='state']"));
           
           WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));

            Func<IWebDriver, IWebElement> waitForElement = new Func<IWebDriver, IWebElement>((IWebDriver Web) =>
            {
                selectState.Click();
                Actions act = new Actions(driver);
                act.SendKeys(Keys.ArrowDown).SendKeys(Keys.ArrowDown).SendKeys(Keys.Enter).Perform();
                if (selectState.Text == "NCR")
                    return selectState;
                return null;
            }
           
            )  ;

            wait.Until(waitForElement);

            Console.WriteLine(selectState.Text);



            // Kill the browser
            driver.Close();
        }
    }
}