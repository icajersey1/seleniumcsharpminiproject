﻿using System;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ICAMini.AdhocFolder
{
    public class FluentWait
    {
        /// Fluent Wait Application 
        /// Author : Ruma
        /// Since : 8th July 2020
        
        [Test]
        public void FluentWaitApply()
        
            {
               IWebDriver driver = new ChromeDriver();

                driver.Navigate().GoToUrl("https://toolsqa.com/automation-practice-switch-windows/");

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));

                Func<IWebDriver, IWebElement> waitForElement = new Func<IWebDriver, IWebElement>((IWebDriver Web) =>
                {
                    Console.WriteLine("Waiting for color to change");
                    IWebElement element = Web.FindElement(By.Id("target"));
                    if (element.GetAttribute("style").Contains("red"))
                    {
                        return element;
                    }
                    return null;
                });

                IWebElement targetElement = wait.Until(waitForElement);
                Console.WriteLine("Inner HTML of element is " + targetElement.GetAttribute("innerHTML"));
            
        }
    }
}

