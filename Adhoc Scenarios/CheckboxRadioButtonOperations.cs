﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace ICAMini.AdhocScenarios
{
    public class CheckboxRadioButtonOperations
    {

        [Test]
        public void Test()
        {
            // Create a new instance of the Firefox driver

            IWebDriver driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // Launch the URL
            driver.Url = "https://demoqa.com/automation-practice-form";
            driver.Manage().Window.Maximize();

            // Select the deselected Radio buttons for category Gender (Use Selected method)
            // Storing all the elements under category 'Gender' in the list of WebLements

            IList<IWebElement> rdBtn_Sex = null;

            rdBtn_Sex = driver.FindElements(By.XPath("//*[@id='genterWrapper']/div[2]/child::div"));

            foreach (var elm in rdBtn_Sex)
            {

                Console.WriteLine(elm.Text);

                if (elm.Selected == false)
                {
                    elm.Click();
                    Thread.Sleep(3000);

                }

            }

            // Select the matched Checkbox button for category Hobbies
            // Storing all the elements under category 'Hobbies' in the list of WebLements

            IList<IWebElement> chkBox_Hobby = null;

             chkBox_Hobby = driver.FindElements(By.XPath("//*[@id='hobbiesWrapper']/div[2]/child::div"));

            foreach (var elm1 in chkBox_Hobby)
            {

                Console.WriteLine(elm1.Text);

                    if ((elm1.Text).Equals("Music")) 
                    {
                        elm1.Click();
                        Thread.Sleep(3000);
                        Console.WriteLine("Sports is Checked");

                    }

                
            }

            // Kill the browser

            driver.Close();
        }
    }
}
    

